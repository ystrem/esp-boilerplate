#ifndef Main_h
#define Main_h

#include "Arduino.h"

class Main {
public:
    void setup();
    void loop();
protected:
    virtual void pre_wifi() {}
};
#endif
